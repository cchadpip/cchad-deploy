
# Actualizar y Reconstruir Contenedores con Docker Compose

Para asegurarte de que los contenedores usen la última versión de las imágenes, sigue estos pasos:

1. **Actualizar las imágenes**:
   ```bash
   docker-compose pull
   ```

2. **Reconstruir y levantar los contenedores**:
   ```bash
   docker-compose up --build --force-recreate
   ```

Esto descargará las últimas versiones de las imágenes especificadas en `docker-compose.yml` y reconstruirá los contenedores forzando su recreación.

¡Listo! Ahora tus contenedores estarán usando las versiones más recientes de las imágenes.
